Spring Boot 3.0 / Java17 with ARM64 Support
---

I was having issues building spring boot apps using the Paketo Buildpacks on ARM64 machines.

There's some happenings in the Paketo Buildpacks community as of late published on Dec 12th:

https://dashaun.com/posts/teamwork-makes-the-dream-work-for-multiarch-builder/

Following along with this blog post allowed me to use his builder to produce an image successfully with `bootBuildImage`.

## Spring Boot Build Image on MacBook Pro M1 Max (Arm64)

Currently, If you attempt to build with standard build packs on an ARM64 based machine you'll get an error:

```bash
$ ./gradlew clean assemble bootBuildImage

> Task :bootBuildImage FAILED
Building image 'docker.io/library/spring-boot-arm64-java17:0.0.1-SNAPSHOT'

 > Pulling builder image 'docker.io/paketobuildpacks/builder:base' ..................................................
 > Pulled builder image 'paketobuildpacks/builder@sha256:a58be9c5ba9ef1aa426324b396c06ca9b4edcb05160a5d73fd0d40b88573b0a1'
 > Pulling run image 'docker.io/paketobuildpacks/run:base-cnb' ..................................................
 > Pulled run image 'paketobuildpacks/run@sha256:dd32ea61706e174d594cd5fba4a38f68f1269f1be8caae1b08c1f2f84ac29060'
 > Executing lifecycle version v0.15.2
 > Using build cache volume 'pack-cache-5cbe5692dbc4.build'

 > Running creator
    [creator]     exec /cnb/lifecycle/creator: exec format error

FAILURE: Build failed with an exception.

* What went wrong:
Execution failed for task ':bootBuildImage'.
> Builder lifecycle 'creator' failed with status code 1

* Try:
> Run with --stacktrace option to get the stack trace.
> Run with --info or --debug option to get more log output.
> Run with --scan to get full insights.

* Get more help at https://help.gradle.org

BUILD FAILED in 4s
```

## Use DaShaun Carter's Experimental Builder

If we use DaShaun's builder for bootBuildImage in `build.gradle` like this:

```groovy
tasks.named("bootBuildImage") {
    builder = "dashaun/builder:tiny"
}
```

We can now run `$ ./gradlew bootBuildImage` on ARM64 based machine produces an ARM64 based image!

This worked successfully on 2 machines successfully:

* 2021 MacBook Pro M1 Max
* EC2 Instance (AMI: ami-03a45a5ac837f33b7 / amzn2-ami-kernel-5.10-hvm-2.0.20221210.1-arm64-gp2)

```bash
$ ./gradlew clean assemble bootBuildImage

> Task :bootBuildImage
Building image 'docker.io/edgewood-software/spring-boot-arm64-java17:0.0.1-SNAPSHOT'

 > Pulling builder image 'docker.io/dashaun/builder:tiny' ..................................................
 > Pulled builder image 'dashaun/builder@sha256:5b8a2248cecf7cc95aacd890678efb69af2354bc49d17527c959822d15cfc26e'
 > Pulling run image 'docker.io/dashaun/jammy-run:tiny' ..................................................
 > Pulled run image 'dashaun/jammy-run@sha256:09928a10963df9736a53db19b9d203a8dad10e57bb1356a666d433ce3e476834'
 > Executing lifecycle version v0.15.2
 > Using build cache volume 'pack-cache-e3300b417373.build'

 > Running creator
    [creator]     ===> ANALYZING
    [creator]     Previous image with name "docker.io/edgewood-software/spring-boot-arm64-java17:0.0.1-SNAPSHOT" not found
    [creator]     ===> DETECTING
    [creator]     6 of 24 buildpacks participating
    [creator]     paketo-buildpacks/ca-certificates   3.5.1
    [creator]     paketo-buildpacks/bellsoft-liberica 9.10.1
    [creator]     paketo-buildpacks/syft              1.23.0
    [creator]     paketo-buildpacks/executable-jar    6.5.0
    [creator]     paketo-buildpacks/dist-zip          5.4.0
    [creator]     paketo-buildpacks/spring-boot       5.20.0
    [creator]     ===> RESTORING
    [creator]     ===> BUILDING
    [creator]
    [creator]     Paketo Buildpack for CA Certificates 3.5.1
    [creator]       https://github.com/paketo-buildpacks/ca-certificates
    [creator]       Launch Helper: Contributing to layer
    [creator]         Creating /layers/paketo-buildpacks_ca-certificates/helper/exec.d/ca-certificates-helper
    [creator]
    [creator]     Paketo Buildpack for BellSoft Liberica 9.10.1
    [creator]       https://github.com/paketo-buildpacks/bellsoft-liberica
    [creator]       Build Configuration:
    [creator]         $BP_JVM_JLINK_ARGS           --no-man-pages --no-header-files --strip-debug --compress=1  configure custom link arguments (--output must be omitted)
    [creator]         $BP_JVM_JLINK_ENABLED        false                                                        enables running jlink tool to generate custom JRE
    [creator]         $BP_JVM_TYPE                 JRE                                                          the JVM type - JDK or JRE
    [creator]         $BP_JVM_VERSION              11                                                           the Java version
    [creator]       Launch Configuration:
    [creator]         $BPL_DEBUG_ENABLED           false                                                        enables Java remote debugging support
    [creator]         $BPL_DEBUG_PORT              8000                                                         configure the remote debugging port
    [creator]         $BPL_DEBUG_SUSPEND           false                                                        configure whether to suspend execution until a debugger has attached
    [creator]         $BPL_HEAP_DUMP_PATH                                                                       write heap dumps on error to this path
    [creator]         $BPL_JAVA_NMT_ENABLED        true                                                         enables Java Native Memory Tracking (NMT)
    [creator]         $BPL_JAVA_NMT_LEVEL          summary                                                      configure level of NMT, summary or detail
    [creator]         $BPL_JFR_ARGS                                                                             configure custom Java Flight Recording (JFR) arguments
    [creator]         $BPL_JFR_ENABLED             false                                                        enables Java Flight Recording (JFR)
    [creator]         $BPL_JMX_ENABLED             false                                                        enables Java Management Extensions (JMX)
    [creator]         $BPL_JMX_PORT                5000                                                         configure the JMX port
    [creator]         $BPL_JVM_HEAD_ROOM           0                                                            the headroom in memory calculation
    [creator]         $BPL_JVM_LOADED_CLASS_COUNT  35% of classes                                               the number of loaded classes in memory calculation
    [creator]         $BPL_JVM_THREAD_COUNT        250                                                          the number of threads in memory calculation
    [creator]         $JAVA_TOOL_OPTIONS                                                                        the JVM launch flags
    [creator]         Using Java version 17 extracted from MANIFEST.MF
    [creator]       BellSoft Liberica JRE 17.0.5: Contributing to layer
    [creator]         Downloading from https://github.com/bell-sw/Liberica/releases/download/17.0.5+8/bellsoft-jre17.0.5+8-linux-aarch64.tar.gz
    [creator]         Verifying checksum
    [creator]         Expanding to /layers/paketo-buildpacks_bellsoft-liberica/jre
    [creator]         Adding 124 container CA certificates to JVM truststore
    [creator]         Writing env.launch/BPI_APPLICATION_PATH.default
    [creator]         Writing env.launch/BPI_JVM_CACERTS.default
    [creator]         Writing env.launch/BPI_JVM_CLASS_COUNT.default
    [creator]         Writing env.launch/BPI_JVM_SECURITY_PROVIDERS.default
    [creator]         Writing env.launch/JAVA_HOME.default
    [creator]         Writing env.launch/JAVA_TOOL_OPTIONS.append
    [creator]         Writing env.launch/JAVA_TOOL_OPTIONS.delim
    [creator]         Writing env.launch/MALLOC_ARENA_MAX.default
    [creator]       Launch Helper: Contributing to layer
    [creator]         Creating /layers/paketo-buildpacks_bellsoft-liberica/helper/exec.d/active-processor-count
    [creator]         Creating /layers/paketo-buildpacks_bellsoft-liberica/helper/exec.d/java-opts
    [creator]         Creating /layers/paketo-buildpacks_bellsoft-liberica/helper/exec.d/jvm-heap
    [creator]         Creating /layers/paketo-buildpacks_bellsoft-liberica/helper/exec.d/link-local-dns
    [creator]         Creating /layers/paketo-buildpacks_bellsoft-liberica/helper/exec.d/memory-calculator
    [creator]         Creating /layers/paketo-buildpacks_bellsoft-liberica/helper/exec.d/security-providers-configurer
    [creator]         Creating /layers/paketo-buildpacks_bellsoft-liberica/helper/exec.d/jmx
    [creator]         Creating /layers/paketo-buildpacks_bellsoft-liberica/helper/exec.d/jfr
    [creator]         Creating /layers/paketo-buildpacks_bellsoft-liberica/helper/exec.d/security-providers-classpath-9
    [creator]         Creating /layers/paketo-buildpacks_bellsoft-liberica/helper/exec.d/debug-9
    [creator]         Creating /layers/paketo-buildpacks_bellsoft-liberica/helper/exec.d/nmt
    [creator]         Creating /layers/paketo-buildpacks_bellsoft-liberica/helper/exec.d/openssl-certificate-loader
    [creator]       Java Security Properties: Contributing to layer
    [creator]         Writing env.launch/JAVA_SECURITY_PROPERTIES.default
    [creator]         Writing env.launch/JAVA_TOOL_OPTIONS.append
    [creator]         Writing env.launch/JAVA_TOOL_OPTIONS.delim
    [creator]
    [creator]     Paketo Buildpack for Syft 1.23.0
    [creator]       https://github.com/paketo-buildpacks/syft
    [creator]         Downloading from https://github.com/anchore/syft/releases/download/v0.62.1/syft_0.62.1_linux_arm64.tar.gz
    [creator]         Verifying checksum
    [creator]         Writing env.build/SYFT_CHECK_FOR_APP_UPDATE.default
    [creator]
    [creator]     Paketo Buildpack for Executable JAR 6.5.0
    [creator]       https://github.com/paketo-buildpacks/executable-jar
    [creator]       Class Path: Contributing to layer
    [creator]         Writing env/CLASSPATH.delim
    [creator]         Writing env/CLASSPATH.prepend
    [creator]       Process types:
    [creator]         executable-jar: java org.springframework.boot.loader.JarLauncher (direct)
    [creator]         task:           java org.springframework.boot.loader.JarLauncher (direct)
    [creator]         web:            java org.springframework.boot.loader.JarLauncher (direct)
    [creator]
    [creator]     Paketo Buildpack for Spring Boot 5.20.0
    [creator]       https://github.com/paketo-buildpacks/spring-boot
    [creator]       Build Configuration:
    [creator]         $BP_SPRING_CLOUD_BINDINGS_DISABLED   false  whether to contribute Spring Boot cloud bindings support
    [creator]       Launch Configuration:
    [creator]         $BPL_SPRING_CLOUD_BINDINGS_DISABLED  false  whether to auto-configure Spring Boot environment properties from bindings
    [creator]         $BPL_SPRING_CLOUD_BINDINGS_ENABLED   true   Deprecated - whether to auto-configure Spring Boot environment properties from bindings
    [creator]       Creating slices from layers index
    [creator]         dependencies (22.6 MB)
    [creator]         spring-boot-loader (269.4 KB)
    [creator]         snapshot-dependencies (0.0 B)
    [creator]         application (28.0 KB)
    [creator]       Launch Helper: Contributing to layer
    [creator]         Creating /layers/paketo-buildpacks_spring-boot/helper/exec.d/spring-cloud-bindings
    [creator]       Spring Cloud Bindings 1.10.0: Contributing to layer
    [creator]         Downloading from https://repo.spring.io/release/org/springframework/cloud/spring-cloud-bindings/1.10.0/spring-cloud-bindings-1.10.0.jar
    [creator]         Verifying checksum
    [creator]         Copying to /layers/paketo-buildpacks_spring-boot/spring-cloud-bindings
    [creator]       Web Application Type: Contributing to layer
    [creator]         Reactive web application detected
    [creator]         Writing env.launch/BPL_JVM_THREAD_COUNT.default
    [creator]       4 application slices
    [creator]       Image labels:
    [creator]         org.springframework.boot.version
    [creator]     ===> EXPORTING
    [creator]     Adding layer 'paketo-buildpacks/ca-certificates:helper'
    [creator]     Adding layer 'paketo-buildpacks/bellsoft-liberica:helper'
    [creator]     Adding layer 'paketo-buildpacks/bellsoft-liberica:java-security-properties'
    [creator]     Adding layer 'paketo-buildpacks/bellsoft-liberica:jre'
    [creator]     Adding layer 'paketo-buildpacks/executable-jar:classpath'
    [creator]     Adding layer 'paketo-buildpacks/spring-boot:helper'
    [creator]     Adding layer 'paketo-buildpacks/spring-boot:spring-cloud-bindings'
    [creator]     Adding layer 'paketo-buildpacks/spring-boot:web-application-type'
    [creator]     Adding layer 'launch.sbom'
    [creator]     Adding 5/5 app layer(s)
    [creator]     Adding layer 'launcher'
    [creator]     Adding layer 'config'
    [creator]     Adding layer 'process-types'
    [creator]     Adding label 'io.buildpacks.lifecycle.metadata'
    [creator]     Adding label 'io.buildpacks.build.metadata'
    [creator]     Adding label 'io.buildpacks.project.metadata'
    [creator]     Adding label 'org.springframework.boot.version'
    [creator]     Setting default process type 'web'
    [creator]     Saving docker.io/edgewood-software/spring-boot-arm64-java17:0.0.1-SNAPSHOT...
    [creator]     *** Images (d7db7cccfae9):
    [creator]           docker.io/edgewood-software/spring-boot-arm64-java17:0.0.1-SNAPSHOT
    [creator]     Adding cache layer 'paketo-buildpacks/syft:syft'
    [creator]     Adding cache layer 'cache.sbom'

Successfully built image 'docker.io/edgewood-software/spring-boot-arm64-java17:0.0.1-SNAPSHOT'

BUILD SUCCESSFUL in 18s
```

## Inspect the resulting image

Notice `"Architecture": "arm64"` below

```bash
$ docker image inspect docker.io/edgewood-software/spring-boot-arm64-java17:0.0.1-SNAPSHOT

[
    {
        "Id": "sha256:1ff3593c94e256af342d2e54be8eb33e14f6bedf96987bacf0988eff6c6cfea1",
        "RepoTags": [
            "edgewood-software/spring-boot-arm64-java17:0.0.1-SNAPSHOT"
        ],
        "RepoDigests": [],
        "Parent": "",
        "Comment": "",
        "Created": "1980-01-01T00:00:01Z",
        "Container": "",
        "ContainerConfig": {
            "Hostname": "",
            "Domainname": "",
            "User": "",
            "AttachStdin": false,
            "AttachStdout": false,
            "AttachStderr": false,
            "Tty": false,
            "OpenStdin": false,
            "StdinOnce": false,
            "Env": null,
            "Cmd": null,
            "Image": "",
            "Volumes": null,
            "WorkingDir": "",
            "Entrypoint": null,
            "OnBuild": null,
            "Labels": null
        },
        "DockerVersion": "",
        "Author": "",
        "Config": {
            "Hostname": "",
            "Domainname": "",
            "User": "",
            "AttachStdin": false,
            "AttachStdout": false,
            "AttachStderr": false,
            "Tty": false,
            "OpenStdin": false,
            "StdinOnce": false,
            "Env": [
                "PATH=/cnb/process:/cnb/lifecycle:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin",
                "CNB_LAYERS_DIR=/layers",
                "CNB_APP_DIR=/workspace",
                "CNB_PLATFORM_API=0.10",
                "CNB_DEPRECATION_MODE=quiet"
            ],
            "Cmd": null,
            "Image": "sha256:73c0cea8d19b287d006ee05f3500be74cf17ab0e7e606100297d060de4aff3c5",
            "Volumes": null,
            "WorkingDir": "/workspace",
            "Entrypoint": [
                "/cnb/process/web"
            ],
            "OnBuild": null,
            "Labels": {
                "io.buildpacks.build.metadata": "...",
                "io.buildpacks.lifecycle.metadata": "...",
                "io.buildpacks.project.metadata": "{}",
                "io.buildpacks.stack.id": "io.paketo.stacks.tiny",
                "org.springframework.boot.version": "3.0.0"
            }
        },
        "Architecture": "arm64",
        "Os": "linux",
        "Size": 181508285,
        "VirtualSize": 181508285,
        "GraphDriver": {
            "Data": {
                "LowerDir": "...",
                "MergedDir": "/var/lib/docker/overlay2/13b8f791fab6fc2ded6b0054ed7e5d88b7d4efeb333450a539e103d0fb71f1df/merged",
                "UpperDir": "/var/lib/docker/overlay2/13b8f791fab6fc2ded6b0054ed7e5d88b7d4efeb333450a539e103d0fb71f1df/diff",
                "WorkDir": "/var/lib/docker/overlay2/13b8f791fab6fc2ded6b0054ed7e5d88b7d4efeb333450a539e103d0fb71f1df/work"
            },
            "Name": "overlay2"
        },
        "RootFS": {
            "Type": "layers",
            "Layers": [
                "sha256:fb052a78ccddd54169fc6e7302e8569c7aa2e9bb8aa74dbaa33c40104e1ed82d",
                "sha256:e31a06d556e306852694d0294ce76ba2dde80fdea2e928c01a5c9994324ec0b1",
                "sha256:984bc4d74a3032ae14900b65da100e601c14e3a92d093573af5a2acc200d0a7e",
                "sha256:ec0381c8f32136ad9564b114b2271d1181e0c181957acb6707e6ff4713a7a89d",
                "sha256:dc559878a0fb5c0c57a940fdfd4773b7515053f7f7e41bcd8253210f977ceebb",
                "sha256:7fbc97c38fad01ae2b8189c8d9a0a0149932192accc4ad253b0f8186d0793e9a",
                "sha256:7e4ddc1834ecc7c1c6fa8dad6b60bb3d89110d99e72684d1b9ed32cd2832fe0c",
                "sha256:33df858545b5408f86e2adc84c9d6696fd3f3ab16065d01a9c3e3366de12a462",
                "sha256:fcc507beb4ccdbc112909bcf999e90802212e4fc86afa20617a863e89081a6da",
                "sha256:eafbfe3d43cf67667fa4b6d100ffe86b98dc8206b6ee4a5543b8b5df6340e391",
                "sha256:0e660565f9768c188d13d6b05d4170fefdcc8256a0e76d6709aa5e93c31b7632",
                "sha256:36ec3bfbf7339129aed254ef3a38e0b367f7bb2dfa83d87d6232e35210e81ef9",
                "sha256:5f70bf18a086007016e948b04aed3b82103a36bea41755b6cddfaf10ace3c6ef",
                "sha256:e0cf3aa7fc800d4dcf63eeac3755be56f94645a02af7b70672bd188de62b8e77",
                "sha256:5f70bf18a086007016e948b04aed3b82103a36bea41755b6cddfaf10ace3c6ef",
                "sha256:b0511a55ddccfad169d13afe55983640c6db6341a3134bc59aa6c2447c39cbd0",
                "sha256:f8e288cbabf8d24d1c507ea1b6415d431715f97170f2d5e402201c1f04e684ca",
                "sha256:1dc94a70dbaa2171fb086500a5d27797f779219b126b0a1eebb9180c2792e80e"
            ]
        },
        "Metadata": {
            "LastTagTime": "2022-12-18T02:03:44.219083069Z"
        }
    }
]
```
## Run the docker container

```bash
$ docker run -it -p 8080:8080 docker.io/deanpoulin/arm64-demo:0.0.1

Setting Active Processor Count to 5
Calculating JVM memory based on 7208124K available memory
For more information on this calculation, see https://paketo.io/docs/reference/java-reference/#memory-calculator
Calculated JVM Memory Configuration: -XX:MaxDirectMemorySize=10M -Xmx6817957K -XX:MaxMetaspaceSize=82966K -XX:ReservedCodeCacheSize=240M -Xss1M (Total Memory: 7208124K, Thread Count: 50, Loaded Class Count: 12234, Headroom: 0%)
Enabling Java Native Memory Tracking
Adding 124 container CA certificates to JVM truststore
Spring Cloud Bindings Enabled
Picked up JAVA_TOOL_OPTIONS: -Djava.security.properties=/layers/paketo-buildpacks_bellsoft-liberica/java-security-properties/java-security.properties -XX:+ExitOnOutOfMemoryError -XX:ActiveProcessorCount=5 -XX:MaxDirectMemorySize=10M -Xmx6817957K -XX:MaxMetaspaceSize=82966K -XX:ReservedCodeCacheSize=240M -Xss1M -XX:+UnlockDiagnosticVMOptions -XX:NativeMemoryTracking=summary -XX:+PrintNMTStatistics -Dorg.springframework.cloud.bindings.boot.enable=true

  .   ____          _            __ _ _
 /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
 \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
  '  |____| .__|_| |_|_| |_\__, | / / / /
 =========|_|==============|___/=/_/_/_/
 :: Spring Boot ::                (v3.0.0)

2022-12-18T03:21:40.897Z  INFO 1 --- [           main] c.e.arm64demo.DemoApplication            : Starting DemoApplication using Java 17.0.5 with PID 1 (/workspace/BOOT-INF/classes started by root in /workspace)
2022-12-18T03:21:40.900Z  INFO 1 --- [           main] c.e.arm64demo.DemoApplication            : No active profile set, falling back to 1 default profile: "default"
2022-12-18T03:21:41.661Z  INFO 1 --- [           main] o.s.b.a.e.web.EndpointLinksResolver      : Exposing 1 endpoint(s) beneath base path '/actuator'
2022-12-18T03:21:41.876Z  INFO 1 --- [           main] o.s.b.web.embedded.netty.NettyWebServer  : Netty started on port 8080
2022-12-18T03:21:41.890Z  INFO 1 --- [           main] c.e.arm64demo.DemoApplication            : Started DemoApplication in 1.218 seconds (process running for 1.43)

```

## Multi-Architecture Docker Manifests

New features in Docker allow us to create versioned (tagged) manifests that contain images targeting specific architectures.

This allows us to have docker automatically figure out which image to use based on the architecture.

For this scenario I created 2 EC2 instances. One a t3.small (amd64) and the other a t4g.small (arm64). I built an image
on each machine and created a manifest that links them all together like this:

### Executed on the x86 EC2 Instance

```bash
# Clone the repository
$ git clone git@gitlab.com:deanwpoulin/spring-boot-arm64-demo.git
$ cd spring-boot-arm64-demo

# Build the Java app and leverage bootBuildImage to create the docker image
$ ./gradlew -Pversion=0.0.2 clean assemble bootBuildImage

# Tag the x86 image and push it
$ docker tag docker.io/deanpoulin/arm64-demo:0.0.2 docker.io/deanpoulin/arm64-demo:0.0.2-x86
$ docker push docker.io/deanpoulin/arm64-demo:0.0.2-x86

```

### Executed on the amd64 EC2 Instance

```bash
# Clone the repository
$ git clone git@gitlab.com:deanwpoulin/spring-boot-arm64-demo.git
$ cd spring-boot-arm64-demo

# Build the Java app and leverage bootBuildImage to create the docker image
$ ./gradlew -Pversion=0.0.2 clean assemble bootBuildImage

# Tag the x86 image and push it
$ docker tag docker.io/deanpoulin/arm64-demo:0.0.2 docker.io/deanpoulin/arm64-demo:0.0.2-arm64
$ docker push docker.io/deanpoulin/arm64-demo:0.0.2-arm64
```

### Create Manifest

Now that the images for x86 and arm64 have been built, we will create a manifest and push that.

It doesn't matter which machine these commands are executed on since the images have been pushed.

```bash
# Create the manifest for the new version
$ docker manifest create docker.io/deanpoulin/arm64-demo:0.0.2 \
    docker.io/deanpoulin/arm64-demo:0.0.2-x86  \
    docker.io/deanpoulin/arm64-demo:0.0.2-arm64

# Annotate the ARM64 image in the manifest with the architecture
$ docker manifest annotate --arch arm64 docker.io/deanpoulin/arm64-demo:0.0.2 \
      docker.io/deanpoulin/arm64-demo:0.0.2-arm64

# Push the manifest to docker hub
$ docker manifest push docker.io/deanpoulin/arm64-demo:0.0.2

# Create the manifest tagged as latest
$ docker manifest create docker.io/deanpoulin/arm64-demo:latest \
    docker.io/deanpoulin/arm64-demo:0.0.2-x86  \
    docker.io/deanpoulin/arm64-demo:0.0.2-arm64

# Annotate the ARM64 image in the manifest with the architecture
$ docker manifest annotate --arch arm64 docker.io/deanpoulin/arm64-demo:latest \
      docker.io/deanpoulin/arm64-demo:0.0.2-arm64

# Push the latest manifest to DockerHub
$ docker manifest push docker.io/deanpoulin/arm64-demo:latest
```

_Note: This process is supported by AWS ECR and DockerHub, the commands are the same._

Take a look at what DockerHub looks like now (see tags):
https://hub.docker.com/r/deanpoulin/arm64-demo/tags

## Run on Any Architecture

Now that we've created the manifests with both architectures present. We can run them on either architecture without having
to explicitly specify the architecture.

For example, I can run this on my Arm based 2021 MacBook Pro M1 Max successfully:

```bash
$ docker run deanpoulin/arm64-demo
Unable to find image 'deanpoulin/arm64-demo:latest' locally
latest: Pulling from deanpoulin/arm64-demo
6f7a97448359: Already exists
913ef9701b42: Already exists
81aa87db95eb: Already exists
5c96df4fa744: Already exists
f319afb33808: Already exists
9ad49796e8f6: Already exists
edc8d518c441: Already exists
02897775ee35: Already exists
e2fea22f3a9e: Already exists
50c0ec858ca5: Already exists
c7a5bf692c9a: Pull complete
2203f507ee91: Pull complete
4f4fb700ef54: Pull complete
6b86f0e6f2bb: Pull complete
2b93f24d4b5d: Pull complete
0d3ec930b5f1: Pull complete
3a4cb709502c: Pull complete
Digest: sha256:e17de751a8b63100a6d43b48b8ebb2eb86cd4db9c3e28039e1e74de418142359
Status: Downloaded newer image for deanpoulin/arm64-demo:latest
Setting Active Processor Count to 5
Calculating JVM memory based on 7221308K available memory
For more information on this calculation, see https://paketo.io/docs/reference/java-reference/#memory-calculator
Calculated JVM Memory Configuration: -XX:MaxDirectMemorySize=10M -Xmx6831141K -XX:MaxMetaspaceSize=82966K -XX:ReservedCodeCacheSize=240M -Xss1M (Total Memory: 7221308K, Thread Count: 50, Loaded Class Count: 12234, Headroom: 0%)
Enabling Java Native Memory Tracking
Adding 124 container CA certificates to JVM truststore
Spring Cloud Bindings Enabled
Picked up JAVA_TOOL_OPTIONS: -Djava.security.properties=/layers/paketo-buildpacks_bellsoft-liberica/java-security-properties/java-security.properties -XX:+ExitOnOutOfMemoryError -XX:ActiveProcessorCount=5 -XX:MaxDirectMemorySize=10M -Xmx6831141K -XX:MaxMetaspaceSize=82966K -XX:ReservedCodeCacheSize=240M -Xss1M -XX:+UnlockDiagnosticVMOptions -XX:NativeMemoryTracking=summary -XX:+PrintNMTStatistics -Dorg.springframework.cloud.bindings.boot.enable=true

  .   ____          _            __ _ _
 /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
 \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
  '  |____| .__|_| |_|_| |_\__, | / / / /
 =========|_|==============|___/=/_/_/_/
 :: Spring Boot ::                (v3.0.0)

2022-12-19T01:09:37.060Z  INFO 1 --- [           main] c.deanpoulin.arm64demo.DemoApplication   : Starting DemoApplication using Java 17.0.5 with PID 1 (/workspace/BOOT-INF/classes started by root in /workspace)
2022-12-19T01:09:37.063Z  INFO 1 --- [           main] c.deanpoulin.arm64demo.DemoApplication   : No active profile set, falling back to 1 default profile: "default"
2022-12-19T01:09:37.787Z  INFO 1 --- [           main] o.s.b.a.e.web.EndpointLinksResolver      : Exposing 1 endpoint(s) beneath base path '/actuator'
2022-12-19T01:09:37.979Z  INFO 1 --- [           main] o.s.b.web.embedded.netty.NettyWebServer  : Netty started on port 8080
2022-12-19T01:09:37.992Z  INFO 1 --- [           main] c.deanpoulin.arm64demo.DemoApplication   : Started DemoApplication in 1.134 seconds (process running for 1.335)
```

