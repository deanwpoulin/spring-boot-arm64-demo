package com.deanpoulin.arm64demo.model;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@Builder
@ToString
public class Greeting {
    private String message;
}
